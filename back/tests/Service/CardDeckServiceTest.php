<?php
namespace App\Tests\Service;

use App\Entity\Card;
use App\Service\CardDeckService;
use PHPUnit\Framework\TestCase;

class CardDeckServiceTest extends TestCase
{
    public function testGetDeck()
    {
        $cardService = new CardDeckService();

        // Test with 10 cards
        $result = $cardService->getDeck(false,10);
        $this->assertIsArray($result);
        $this->assertCount(10, $result);

        // Test with no count params
        $result = $cardService->getDeck(false, null);
        $this->assertIsArray($result);
        $this->assertCount($cardService->totalCards(), $result);
    }

    public function testSortCards()
    {
        $cardService = new CardDeckService();

        $cardData = [
            ['value' => 5, 'color' => 'Cœur'],
            ['value' => 2, 'color' => 'Cœur'],
            ['value' => 'AS', 'color' => 'Cœur'],
            ['value' => 4, 'color' => 'Pique'],
            ['value' => 2, 'color' => 'Trèfle'],
            ['value' => 5, 'color' => 'Carreaux'],
            ['value' => 4, 'color' => 'Carreaux'],
        ];

        $cards = [];
        foreach ($cardData as $data) {
            $card = new Card();
            $card->setColor($data['color']);
            $card->setValue($data['value']);
            $cards[] = $card;
        }

        $cardService->sortCards($cards);

        $cardsDataSorted = [
            ['value' => 4, 'color' => 'Carreaux'],
            ['value' => 5, 'color' => 'Carreaux'],
            ['value' => 'AS', 'color' => 'Cœur'],
            ['value' => 2, 'color' => 'Cœur'],
            ['value' => 5, 'color' => 'Cœur'],
            ['value' => 4, 'color' => 'Pique'],
            ['value' => 2, 'color' => 'Trèfle'],
        ];

        foreach ($cards as $i => $card) {
            $this->assertEquals($card->getColor(), $cardsDataSorted[$i]['color']);
            $this->assertEquals($card->getValue(), $cardsDataSorted[$i]['value']);
        }
    }
}