<?php

use App\Entity\Card;
use PHPUnit\Framework\TestCase;

/**
 * Test entity Card
 */
class CardTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $card = new Card();

        $card->setValue('5');
        $card->setColor('Coeur');
        $this->assertEquals('5', $card->getValue());
        $this->assertEquals('Coeur', $card->getColor());

        $card->setValue('AS');
        $card->setColor('Pique');
        $this->assertEquals('AS', $card->getValue());
        $this->assertEquals('Pique', $card->getColor());
    }
}