<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CardRepository;

#[ORM\Entity(repositoryClass: CardRepository::class)]
class Card
{
    #[ORM\Column(length: 10)]
    private ?string $color = null;

    #[ORM\Column(length: 10)]
    private ?string $value = null;

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string $color
     *
     * @return Card
     */
    public function setColor(string $color): Card
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return Card
     */
    public function setValue(string $value): Card
    {
        $this->value = $value;

        return $this;
    }
}
