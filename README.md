# Front
## Angular 18
* cd front
* npm install
* npm start

# Back
## Symfony 7
* cd back
* composer install
* symfony server:start