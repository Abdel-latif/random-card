import {Component, OnInit, inject} from '@angular/core';
import {Card} from "../card";
import {CardService} from "../card.service";
import {NgClass, NgForOf, NgIf} from "@angular/common";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    NgIf,
    NgForOf,
    NgClass,
    FormsModule
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  private cardService = inject(CardService);
  cards: Card[] | undefined;
  isLoading = false;
  count = 10;
  sort = false;

  ngOnInit(): void {
    this.getCards();
  }

  getCards() {
    this.isLoading = true;
    this.cardService.getCards({
      count: this.count,
      sort: this.sort
    }).subscribe(data => {
      this.cards = data;
      this.isLoading = false;
    });
  }

  designForm(value: string) {
    const form = new Map();
    form.set('Trèfle', '♣');
    form.set('Carreaux', '♦');
    form.set('Pique', '♠');
    form.set('Cœur', '♥');

    return form.get(value);
  }

  sortChange() {
    this.sort = !!this.sort;

    this.getCards();
  }
}
